# django-cmsplugin-objects

## What does it do?

Provides an extra toolbar in the django cms tools, allowing administrators easy
access to the administrator pages for the currently in scope object or object type.

For example, if the page contains a list of blog entries, then you will have the option
to create a new blog entry. Which will pop up the admin form in the in-line django-cms
modal dialog. (Usually when using ListView generic clas sbased views)

If the page contains a single Blog entry object, then an Edit and Delete option will be
made available and these operate much like the Create form.

## Installation

```
pip install cmsplugin-objects
```

Add the plugin to your site's middleware in settings.py::

```
MIDDLEWARE_CLASSES = (
  ...
  'cmsplugin_objects.middleware.ObjectToolbarMiddleware',
  ...
)
```

Then restart your web server.

## Customise Objects List

By default the middleware will look for 'object' and 'object_list' in the data context of each response. But sometimes you may have views where you have different objects (or multiple objects) for these, you can specify a list of keys in the context data called 'object-bar', each entry should be a string which is a key back into the context of the object to use as the template for the menu.

```
def get_context_data(self, **kwargs):
    data = super(MyView, self).get_context_data(**kwargs)
    data['obj1'] = MyObject.objects.get(pk=1)
    data['obj2'] = MyOtherObject.objects.all()
    data['object-bar'] = ['obj1', 'obj2']
    return data
```

## Issues

Please submit issues and merge requests at GitHub issues tracker: https://github.com/doctormo/django-cmsplugin-objects/issues/.

