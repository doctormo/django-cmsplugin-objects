#!/usr/bin/env python
#
# Copyright (C) 2017 Martin Owens
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#

from setuptools import setup
import os

# Grab description for Pypi
with open('README.md') as fhl:
    description = fhl.read()

setup(
    name             = "cmsplugin-objects",
    version          = "1.1",
    description      = "Extend django-cms with admin form links, editing object from the toolbar",
    long_description = description,
    author           = 'Martin Owens',
    url              = 'https://github.com/doctormo/django-cmsplugin-objects',
    author_email     = 'doctormo@gmail.com',
    platforms        = 'linux',
    license          = 'LGPLv3',
    packages         = ['cmsplugin_objects'],
    install_requires = ['django-cms>=3.4.4'],
    classifiers      = [
      'Development Status :: 5 - Production/Stable',
      'Development Status :: 6 - Mature',
      'Intended Audience :: Developers',
      'Intended Audience :: Information Technology',
      'Intended Audience :: System Administrators',
      'Programming Language :: Python',
      'Programming Language :: Python :: 2.7',
    ],
 )

